library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity program_counter is
     port(
          Cp: in std_logic;
          Ep: in std_logic;
          clk: in std_logic;
          clr: in std_logic;
          outbuff: out std_logic_vector(3 downto 0));
end program_counter;

architecture behavioral of program_counter is
     signal outbuff_aux: std_logic_vector(3 downto 0);
begin
     process(Cp,Ep,clk,clr)
     begin
          if(Ep='1') then
               if(clk'event and clk='0') then
                    if(clr='0') then
                         outbuff_aux<="0000";
                    else
                         outbuff_aux<=outbuff_aux+1;
                    end if;
               end if;
          else
               outbuff_aux<="ZZZZ";
          end if;
     end process;
     outbuff<=outbuff_aux;
end behavioral;

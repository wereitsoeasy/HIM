This is a villain so evil, so sinister, so horribly vile that even the utterance of his name strikes fear into the hearts of men. The only safe way to refer to this King of Darkness is simply "Him!"

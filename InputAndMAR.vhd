----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    04:39:44 06/08/2018 
-- Design Name: 
-- Module Name:    MARAndInput - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MARAndInput is
    Port ( d : in  STD_LOGIC_VECTOR (3 downto 0);
           lm : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           s1 : in  STD_LOGIC_VECTOR (3 downto 0);
           s2 : in  STD_LOGIC;
           o : out  STD_LOGIC_VECTOR (3 downto 0));
end MARAndInput;

architecture Behavioral of MARAndInput is
signal MARQaux : std_logic_vector(3 downto 0);
begin
memoria: process(d, lm, clk)
begin
	if(clk'event and clk = '1')then
		if(lm = '0')then
			--escribir a memoria
			MARQaux <= d;
		end if;
	end if;
end process;

mux: process(s1, s2)
begin
	if(s2 = '0')then -- PROG switch register
		o <= s1;
	else --RUN MAR register
		o <= MARQaux;
	end if;
end process;
end Behavioral;


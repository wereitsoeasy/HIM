library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

entity ram is
    Port ( cs : in  STD_LOGIC;
           rw : in  STD_LOGIC;
           a : in  STD_LOGIC_VECTOR (3 downto 0);
           i : in  STD_LOGIC_VECTOR (7 downto 0);
           o : out  STD_LOGIC_VECTOR (7 downto 0));
end ram;

architecture Behavioral of ram is
type ram is array(127 downto 0) of std_logic_vector(7 downto 0);
signal dato: ram;
signal clk_w, clk_r: std_logic;
begin
	clk_w <= ((not cs) and (not rw));
	clk_r <= ((not cs) and (rw));
	process(clk_w, clk_r, cs, a, i, dato) is
	begin
		if(clk_w'event and clk_w = '1') then
			dato(conv_integer(a)) <= i;
		elsif(clk_r'event and clk_r = '1') then
			o <= dato(conv_integer(a));
		else o <= "--------";
		end if;
	end process;
end Behavioral;
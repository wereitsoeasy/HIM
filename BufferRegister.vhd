library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity buffer_register is
     port(
          clk,lb: in std_logic;
          input: in std_logic_vector(7 downto 0);
          output: out std_logic_vector(7 downto 0));
end buffer_register;

architecture behavioral of buffer_register is
signal tmp: std_logic_vector(7 downto 0);
begin
     process(clk,lb,input)
     begin
          if (clk'event and clk='1') then
               if lb='1' then
                    tmp<=tmp;
               else
                    tmp<=input;
               end if;
          end if;
     end process;
     output<=tmp;
end behavioral;
